FROM adoptopenjdk/openjdk11:jre-11.0.8_10-alpine

LABEL maintainer="ONAP Integration team, morgan.richomme@orange.com"
LABEL Description="Reference ONAP JAVA 11 image"

ENV JAVA_OPTS="-Xms256m -Xmx1g"
ENV JAVA_SEC_OPTS=""

ARG user=onap
ARG group=onap

# Create a group and user
RUN addgroup -S $group && adduser -S -D -h /usr/$user $user $group && \
    chown -R $user:$group /usr/$user &&  \
    mkdir /var/log/$user && \
    mkdir /app && \
    chown -R $user:$group /var/log/$user && \
    chown -R $user:$group /app


# Tell docker that all future commands should be run as the onap user
USER $user
WORKDIR /usr/$user

ENTRYPOINT exec java $JAVA_SEC_OPTS $JAVA_OPTS -jar /app/app.jar
